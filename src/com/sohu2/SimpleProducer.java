package com.sohu2;

import java.util.Properties;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import com.sohu.kafkademon.KafkaProperties;

public class SimpleProducer {
	private static Producer<Integer, String> producer;
	private final Properties props = new Properties();

	public SimpleProducer() {
		props.put("metadata.broker.list", "192.168.200.252:9092");
		props.put("serializer.class", "kafka.serializer.StringEncoder");
		props.put("request.required.acks", "1");
		producer = new Producer<Integer, String>(new ProducerConfig(props));
	}

	public static void main(String[] args) {
//		String topic = (String) args[0];
//		String messageStr = (String) args[1];

		new SimpleProducer();
		
		String topic = KafkaProperties.topic;
		String messageStr = new String("Message_" + 100);
		System.out.println("Send:" + messageStr);
		KeyedMessage<Integer, String> data = new KeyedMessage<Integer, String>(topic, messageStr);
		producer.send(data);
		producer.close();
	}
}