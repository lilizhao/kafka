package com.sohu.kafkamain;

import com.sohu.kafkademon.KafkaProducer;
import com.sohu.kafkademon.KafkaProperties;

/**
 * 
 * @author sks
 *
 */
public class KafkaProducerDemo {
	public static void main(String[] args) {
		KafkaProducer producerThread = new KafkaProducer(KafkaProperties.topic);
		producerThread.start();
	}
}
