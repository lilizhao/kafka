package com.sohu.kafkamain;

import com.sohu.kafkademon.KafkaConsumer;
import com.sohu.kafkademon.KafkaProperties;

/**
 * 
 * @author sks
 *
 */
public class KafkaConsumerDemo {
	public static void main(String[] args) {
		KafkaConsumer consumerThread = new KafkaConsumer(KafkaProperties.topic);
		consumerThread.start();
	}
}
